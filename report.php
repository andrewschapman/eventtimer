<?php
require_once("includes/setup.php");

try {
    // Get an Event Manager instance
    $objEventManager = \UserReplay\EventManager::getInstance();
    
    // Just for gags lets track how long the stats take to calculate.
    $objEventManager->trackEvent("Calculate Stats");
    
    // Calculate the status for the timers we want to report on
    $stats["items"]["Download Google Homepage"] = $objEventManager->calculateStats("Google");
    $stats["items"]["Run Fibonacci Sequence"] = $objEventManager->calculateStats("Fibonacci");
    $stats["items"]["Decode JSON Data"] = $objEventManager->calculateStats("JSON Decode");
    $stats["items"]["Unserialize Array Data"] = $objEventManager->calculateStats("Unserialize");
    
    $objEventManager->eventFinished("Calculate Stats");    
    
    $stats["items"]["Calculate Stats"] = $objEventManager->calculateStats("Calculate Stats");
    
    // Pass the timer stats into our view file and show it.
    Utils::loadView("report.php", $stats);
    
} catch(Exception $e) {
    print "The following problem occured: " . $e->getMessage() . "<br>";
}