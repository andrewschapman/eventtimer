<!DOCTYPE html>
<html>
<head>
    <meta name="author" content="Andrew Chapman">
    <title>Timer Report</title>
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
    <?php if(isset($items)) : ?>
    google.load('visualization', '1', {packages: ['corechart', 'bar']});
    google.setOnLoadCallback(drawBasic);

    function drawBasic() {

    var data = google.visualization.arrayToDataTable([
        ['Timer', 'ms',],
        <?php foreach($items as $name => $values) : ?>
        ['<?php echo $name; ?>', <?php echo $values["average"]; ?>],
        <?php endforeach; ?>
        ]);

        var options = {
            title: 'Execution time in milliseconds',
            chartArea: {width: '50%'},
            hAxis: {
                title: 'Execution time',
                minValue: 0
            },
            vAxis: {
                title: 'Timer'
            }
        };

        var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
        
        chart.draw(data, options);
    }    
    <?php endif; ?>
    </script>
</head>
<body>
    <h1>Timer Report</h1>
    
    <table>
        <thead>
            <tr>
                <th>Timer name</th>
                <th>No. Entries</th>
                <th>Average (ms)</th>
                <th>Standard Deviation</th>
                <th>Min time</th>
                <th>Max time</th>
            </tr>
        </thead>
        <tbody>
        <?php
            if(isset($items)) {
                foreach($items as $name => $values) {
                    ?>
            <tr>
                <td><?php echo $name; ?></td>
                <td><?php echo $values["total_entries"]; ?></td>
                <td><?php echo $values["average"]; ?></td>
                <td><?php echo $values["standard_deviation"]; ?></td>
                <td><?php echo $values["min"]; ?></td>
                <td><?php echo $values["max"]; ?></td>
            </tr>
                    <?php
                }
            }
        ?>
        </tbody>
    </table>
    
    <div id="chart_div"></div>
</body>
</html>
