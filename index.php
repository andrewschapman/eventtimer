<?php
require_once("includes/setup.php");

try {
    // Get an Event Manager instance and start tracking our events
    $objEventManager = \UserReplay\EventManager::getInstance();

    // Download Google's home page, tracking the time that takes
    $objEventManager->trackEvent("Google");
    Utils::downloadGoogleHomepage();
    $objEventManager->eventFinished("Google");
    
    // Calculate the fibonacci sequence numbers for the
    // the numbers between 1 and 100,000 and track how long that takes.
    $objEventManager->trackEvent("Fibonacci");
    Utils::fibonacciTest();
    $objEventManager->eventFinished("Fibonacci");
    
    // Test the speed of decoding JSON data
    $objEventManager->trackEvent("JSON Decode");
    Utils::JSONDecodeTest();
    $objEventManager->eventFinished("JSON Decode");    
    
    // Test the speed of unserializing array data
    $objEventManager->trackEvent("Unserialize");
    Utils::UnserializeTest();
    $objEventManager->eventFinished("Unserialize");        

    // Print time taken for each individual event
    print "Time taken to calculate Fibonacci: " . $objEventManager->getEventTime("Fibonacci") . "<br>";
    print "Time taken to download Google's homepage: " . $objEventManager->getEventTime("Google") . "<br>";
    print "Time taken to decode JSON data: " . $objEventManager->getEventTime("JSON Decode") . "<br>";
    print "Time taken to unserialize array data: " . $objEventManager->getEventTime("Unserialize") . "<br>";
    
    // Script finished, calculate total execution time.
    print "The total time taken for all timers was: " .$objEventManager->calcTotalTime() . "<br>";
    print "The total page execution time was: " .$objEventManager->getPageExecutionTime() . "<br>";
    
} catch(Exception $e) {
    print "The following problem occured: " . $e->getMessage() . "<br>";
}