<?php
// CLASS AUTOLOADER ***********************************************/
function __autoload($class_name) {
    $path = "classes/" . strtolower(str_replace("\\", "/", $class_name)) . '.class.php';
    require_once($path);
}

// Define absolute path
define("ABSOLUTE_PATH", str_replace("/includes", "", dirname(__FILE__)));

// Define log file directory
define("LOG_FILE_DIR", ABSOLUTE_PATH . "/logs/events/");

