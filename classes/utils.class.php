<?php
class Utils
{
    /**
    * Testing the speed of calculating fibonaci sequence numbers
    */    
    public static function fibonacciTest()
    {
        for($x = 1; $x < 100000; $x++) {            
            $result = round(pow((sqrt(5)+1)/2, $x) / sqrt(5));
        }  
    }
    
    /**
    * Downloads the HTML from Google's home page.
    */
    public static function downloadGoogleHomepage()
    {
        $contents = file_get_contents("http://www.google.co.uk");
    } 
    
    /**
    * Test for JSON Data decoding speed
    */
    public static function JSONDecodeTest()
    {
        $jsonData = '{first_name: "Andrew", last_name: "Chapman", phone: "07943 651 675" }';
        
        for($x = 0; $x < 100000; $x++) {
            $data = json_decode($jsonData);
        }
    } 

    /**
    * Test for unserializing array data speed.
    */    
    public static function UnserializeTest()
    {
        $arrayData = 'a:3:{s:10:"first_name";s:6:"Andrew";s:9:"last_name";s:7:"Chapman";s:5:"phone";s:13:"07943 651 675";}';
        
        for($x = 0; $x < 100000; $x++) {
            $data = unserialize($arrayData);
        }
    }
    
    /**
    * Calculates the standard deviation of the values passed in the array
    * 
    * @param array $aValues Array of numeric values
    * @return float
    */
    public function StandardDeviation($aValues)
    {
        $fMean = array_sum($aValues) / count($aValues);
        $fVariance = 0.0;
        
        foreach ($aValues as $i) {
            $fVariance += pow($i - $fMean, 2);
        }
        
        $fVariance /= count($aValues);
        return (float) sqrt($fVariance);
    }
    
    /**
    * Rudimentary view loader.
    * 
    * @param string $viewFile The name of the view file to load
    * @param array $viewData The data to pass to the view.
    */
    public function loadView($viewFile, $viewData = array())
    {
        // Extract variables in the array into local scope.
        extract($viewData);
        
        // Load the view file and capture the output.
        ob_start();
        include(ABSOLUTE_PATH . "/views/" . $viewFile);
        $result = ob_get_contents();
        ob_end_clean();

        echo $result;
    }        
}
