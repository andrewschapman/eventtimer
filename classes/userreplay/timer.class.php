<?php
namespace UserReplay; 

/**
* The Timer class is used to track the start and finish time of a single event,
* and also to calculate the timer / event duration.
*/
class Timer
{
    private $startTime;
    private $finishTime;
    private $duration;
    private $finished;
    
    /**
    * Constructor method initialises class variables
    * and starts the timer automatically.
    */
    public function __construct()
    {
        $this->startTime = microtime(true);
        $this->finishTime = 0;
        $this->duration = 0;
        $this->finished = false;
    }
    
    /**
    * The finsihed method signals that the event has finished
    * and the timer duration should be calculated.
    * 
    * @return float The timer / event duration
    */
    public function finished()
    {
        if(!$this->finished) {
            $this->finished = true;
            $this->finishTime = microtime(true);
            $this->duration = $this->finishTime - $this->startTime;
        }
        
        return $this->duration;
    }
    
    /**
    * Returns the total timer duration.
    */
    public function getDuration()
    {
        return $this->finished();
    }
}
