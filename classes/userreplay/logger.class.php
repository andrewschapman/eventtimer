<?php
namespace UserReplay; 

/**
* Logs data in JSON encoding
* into a log file.
*/
class Logger
{
    private $filePath;   // The full path to the log file.
    
    /**
    * @param string $dir Directory name to write the log file to.
    * @param string $fileName The filename to use for the log file (note, .txt will be added to the end automatically.
    */
    public function __construct($dir, $fileName)
    {
        // Ensure passed directory and filename are valid
        if((empty($dir)) || (empty($fileName))) {
            throw new \Exception("Logger - the directory and filename may not be blank");
        }
        
        // Ensure directory is present, and if not, attempt to create it.
        if(!is_dir($dir)) {
            @mkdir($dir);
            if(!is_dir($dir)) {
                throw new \Exception("Log directory $dir doesn't exist and couldn't create it.  Please check the parent path and file permissions.");    
            }
        }
        
        // If the directory path doesn't have a trailing slash, add a slash to the path.
        if(substr($dir, strlen($dir) - 1, 1) != "/") {
            $dir .= "/";
        }
        
        // Ensure filenames only contain characters we want to allow.
        $fileName = preg_replace("~[^a-z0-9:.]~i", "", $fileName); 
        
        // Store the full path to the file and if it doesn't already exist, attempt to create it.
        $this->filePath = $dir . $fileName;
        
        if(!file_exists($this->filePath)) {
            $fp = @fopen($this->filePath, "w");
            if(!$fp) {
                throw new \Exception("The log file " . $this->filePath ." cannot be created.  Please check file permissions.");    
            } else {
                fclose($fp);
            }            
        }
    }
    
    /**
    * Writes the passed associative array data to the log file
    * @param array $data
    */
    public function writeToLog($data)
    {
        if(!is_array($data)) {
            throw new \Exception("Logger::writeToLog Data to write must be an array.");       
        }
        
        // Insert time stamp which will allow us to know
        // exactly when an event was recorded for analysis later.
        // Note we could add additional dimensions such as CPU & memory usage
        // which may be of relevance.
        $data["stamp"] = time();
        
        file_put_contents($this->filePath, json_encode($data) . "\n", FILE_APPEND);
    }
    
    /**
    * Loads all the data in the log file
    * @returns A string of all the log data in JSON encoded format.
    */
    public function getLogData()
    {
        $data = file_get_contents($this->filePath);
        return $data;
    }
}
