<?php
/**
* Standard PHP Singleton class definition used to implement
* the singleton design pattern. As this class must always
* be extended (i.e. not used on its own), it's abstract.
*/
namespace UserReplay;  

abstract class Singleton
{
    /**
    * @var Singleton The reference to *Singleton* instance of this class
    */
    protected static $instance;
    
    /**
    * Returns the *Singleton* instance of this class.
    *
    * @return Singleton The *Singleton* instance.
    */
    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }
        
        return static::$instance;
    }

    /**
    * Protected constructor to prevent creating a new instance of the
    * *Singleton* via the `new` operator from outside of this class.
    */
    protected function __construct()
    {
    }

    /**
    * Private clone method to prevent cloning of the instance of the
    * *Singleton* instance.
    *
    * @return void
    */
    private function __clone()
    {
    }

    /**
    * Private wakeup method to prevent unserializing of the *Singleton*
    * instance.
    *
    * @return void
    */
    private function __wakeup()
    {
    }
}