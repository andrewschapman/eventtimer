<?php
namespace UserReplay; 

/**
* Implements event time tracking.
* Supports an unlimited number of named timers
* Total execution time is tracked indepedantly by way of a page timer.
*/
class EventManager extends Singleton
{
    private $pageTimer; // Used to store total page execution
    private $eventHash; // An associative array used to store all other timers
    
    public function __construct()
    {
        $this->pageTimer = new Timer(); // Total execution time timer.
        $this->eventHash = [];  // Used to store individual event timers
    }
    
    /**
    * Start tracking a named event.
    * 
    * @param string $eventName The event name to track.
    */
    public function trackEvent($eventName)
    {
        // Event names may not be blank
        if(empty($eventName)) {
            throw new Exception("EventManager::trackEvent - Event name may not be blank");
        }
        
        // Ensure we're not overwriting an existing event
        if(isset($this->eventHash[$eventName])) {
            throw new Exception("EventManager::validate - Sorry, the event named '$eventName' already exists");
        }
            
        $this->eventHash[$eventName] = new Timer();
    }
    
    /**
    * Use this method to signal that an event has finished.
    * You must pass in exactly the same name as you used
    * to start tracking the event.
    * 
    * @param string $eventName The name of the event to signal as finished.
    * @return float The total time the event took (i.e. finish - start time)
    */
    public function eventFinished($eventName)
    {
        $this->validate($eventName);
        
        $eventDuration = $this->eventHash[$eventName]->finished();        
        
        $objLogger = $this->createLogger($eventName);
        
        $data = [
            "duration" => $eventDuration
        ];
        
        $objLogger->writeToLog($data);
        
        return $eventDuration;
    }    
    
    /**
    * Returns the total page execution time
    * Shouldn't be called until the very end of the script.
    */
    public function getPageExecutionTime()
    {
        return $this->pageTimer->getDuration();
    }    
    
    /**
    * A semantic sugar function - does the same thing as event finished
    * but with a more intuitive name for retreiving event time.
    * 
    * @param mixed $eventName
    */
    public function getEventTime($eventName)
    {
        return $this->eventFinished($eventName);       
    }  
    
    /**
    * Calculates the total time taken for all timers.
    * 
    * @return float The total time taken for all timers in microseconds
    */
    public function calcTotalTime()
    {
        if(count($this->eventHash) == 0) {
            return 0;
        }
        
        $totalTime = 0;
        
        foreach($this->eventHash as $name => $objTimer) {
            $totalTime += $objTimer->getDuration();    
        }
        
        return $totalTime;
    }  
    
    /**
    * Creates a new Logger instance for the specified
    * event name.
    * 
    * @param string $eventName The event name to create the logger for.
    * @return Logger A Logger object.
    */
    private function createLogger($eventName){
        $objLogger = new Logger(LOG_FILE_DIR, $eventName . ".txt");
        
        return $objLogger;
    }
    
    /**
    * Used internally to validate that a named event is valid an 
    * exists in the event hash.
    * 
    * @param string $eventName The event name to check.
    */
    private function validate($eventName)
    {
        // Event names may not be blank
        if(empty($eventName)) {
            throw new Exception("EventManager::validate - Please specify the event you're looking for");
        }
        
        // Event names may not be blank
        if(!isset($this->eventHash[$eventName])) {
            throw new Exception("EventManager::validate - Sorry, the event named '$eventName' does not exist");
        }        
    } 
    
    /**
    * Calculates basic statistics such as average, standard deviation, min and max
    * for the named event.
    * 
    * @param string $eventName The event to calculate the stats for
    * @return The stats in an associative array.
    */
    public function calculateStats($eventName)
    {
        // Setup results array with default values
        $results = [
            "average" => 0,
            "total_entries" => 0,
            "standard_deviation" => 0,
            "max" => 0,
            "min" => 0
        ];
        
        // Get the data for the named log file.
        $objLogger = $this->createLogger($eventName);
        $JSONdata = $objLogger->getLogData();
        
        if(empty($JSONdata)) {
            return false;
        }
        
        // Break the data into individual lines
        $lines = explode("\n", $JSONdata);
        
        if(count($lines) == 0) {
            return;
        }
        
        // Loop through lines and calculate stats
        $total = 0;
        $durations = [];
        
        foreach($lines as $line) {
            $data = json_decode($line);
            if($data && property_exists($data, "duration")) {
                $total += $data->duration;
                $results["total_entries"]++;
                $durations[] = $data->duration;
                
                if($data->duration > $results["max"]) {
                    $results["max"] = $data->duration;
                }
                
                if(($data->duration < $results["min"]) || ($results["min"] == 0)) {
                    $results["min"] = $data->duration;
                }                
            }
        } 
        
        if($results["total_entries"] > 0) {
            $results["average"] = number_format($total / $results["total_entries"], 4);
            $results["standard_deviation"] = number_format(\Utils::StandardDeviation($durations), 4); 
            $results["min"] = number_format($results["min"], 4);
            $results["max"] = number_format($results["max"], 4);
        }
        
        return $results;
    }                     
}
