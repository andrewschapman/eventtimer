Event Timer Demo
=========

## Important

Before testing the code, please create the following directory structure under the main 
project root folder:

logs/events

and please ensure the logs/events folder is writable to the webserver.  Log
files are created in this directory and are used to store duration data for 
each event timer.

## Answering the brief

Please review index.php and set as many timers as you like.  You will see that:

* You can add an unlimited number of timers;
* You get get the total execution time for any timer individually;
* You can get the total execution time for all timers;

I've tried to avoid using any frameworks and as such some of the code for doing things like loading
views is pretty rudimentary.  

## And for extra functionality...

* Once you have a few timers being recorded, please review report.php
* You will note that every time we record the time for an event that the duration is logged for that.
* This allows us to report on each event in a meaningful way, so we can calculate statistics such as averages, standard deviations etc.
* There's also a Google graph to show the timers side by side for comparison.

Cheers.
